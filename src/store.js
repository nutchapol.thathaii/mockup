import { compose, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducer from './redux/reducer'

const myLog = (store) => (next) => (action) => {
  // console.log('Log Action : ', action)
  next(action)
}
const middleware = [thunk, myLog]

export default function configStore() {
  const store = createStore(
    reducer,
    compose(
      applyMiddleware(...middleware)
    )
  )
  return { store }
}
