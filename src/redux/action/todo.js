import { TODO } from '../actionTypes'

const createTodo = (data) => ({
  type: TODO.CREATE_TODO,
  data
})

const updateTodo = (data) => ({
  type: TODO.UPDATE_TODO,
  data
})

const deleteTodo = (id) => ({
  type: TODO.DELETE_TODO,
  id
})

// const getApiList = () => ({
//   type: TODO.GET_API_LIST,
// })

// export const getApiListAction =  (dispatch) => {
//   dispatch(getApiList())
// }

export const createTodoAction = (data) => (dispatch) => {
  dispatch(createTodo(data))
}

export const updateTodoAction = (id, text) => (dispatch) => {
  const data = {
    id, text
  }
  dispatch(updateTodo(data))
}

export const deleteTodoAction = (id) => (dispatch) => {
  dispatch(deleteTodo(id))
}

