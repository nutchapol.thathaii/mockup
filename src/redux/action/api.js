
const getApiList = (data) => ({
    type: 'GET_API_LIST',
    data
  })
  
  export const getApiListAction = () => (dispatch) => {
    fetch('http://localhost:9011/api/agent/mockup/list/api')
      .then(response => response.json())
      .then(json => dispatch(getApiList(json)))
  }