
const getUsers = (data) => ({
  type: 'GET_USERS',
  data
})

export const getUsersAction = () => (dispatch) => {
  fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(json => dispatch(getUsers(json)))
}