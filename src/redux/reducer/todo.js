import { TODO } from '../actionTypes'

const todoReducer = (state = [], action) => {
  switch (action.type) {
    case TODO.CREATE_TODO:
      return [...state, action.data]

    case TODO.UPDATE_TODO:
      let mock = []
      state?.map(item => {
        if (item.id === action?.data?.id) {
          const element = {
            ...item,
            status: 'update',
            text: action?.data?.text
          }
          mock.push(element)
        } else {
          mock.push(item)
        }
      })
      return mock

    case TODO.DELETE_TODO:
      return state?.filter(item => item.id !== action.id)
    default:
      return state
  }
}

export default todoReducer