import { combineReducers } from 'redux'
import todo from './todo'
import users from './users'
import apiList from './apiList'

const appReducer  = combineReducers({
  todo,
  users,
  apiList
})

const rootReducer = (state, action) => {
  return appReducer(state, action)
}

export default rootReducer