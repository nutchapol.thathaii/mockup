import { TODO } from '../actionTypes'
const apiListReducer = (state = [], action) => {
    switch (action.type) {
      case 'GET_API_LIST':
        return  action.data
      default:
        return state
    }
  }
  
  export default apiListReducer