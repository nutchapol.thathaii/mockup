import React, { Component } from 'react'
import { connect } from 'react-redux';
import './App.scss';
import { createTodoAction, updateTodoAction, deleteTodoAction } from './redux/action/todo'
import { getApiListAction } from './redux/action/api'
import logo1 from './assets/m.svg'
import logo2 from './assets/logo.png'
import logo3 from './assets/c.svg'
import remove from './assets/delete.svg'
import logo4 from './assets/k.svg'
import circle from './assets/circle.svg'


import axios from 'axios'
const datas = [
  { aa: 'aa' },
  { aa: 'bb' },
]

class App extends Component {

  static defaultProps = {
    apiLists: []
  }
  constructor() {
    super()
    this.state = {
      dataMock: '',
      method: '',
      case: '',
      path: 'http://localhost:3000/api/agent',
      env: 'http://localhost:3000/api/agent',
      name: '',
      value: '',
      log: [],
      btnStatus: {},
      typing: false,
      api: '',
      trick: '',
    }
  }

  componentDidMount() {
    this.props.apiList()
  }

  // componentDidUpdate() {
  //   this.props.apiList()
  // }

  handleselect = (path, index) => {
    this.setState({ api: index })
    let reqData = {
      path: path.api,
    }
    axios.post(`${this.state.env}/mockup/api/data`, reqData).then(
      response => {
        if (response.status === 200) {
          this.setState({ dataMock: JSON.stringify(response.data.resultData, null, 1) })
        }
      })
  }
  // create = () => {
  //   const data = {
  //     id: Math.random(),
  //     text: this.state.value,
  //     status: 'create'
  //   }

  //   this.props.createTodo(data)
  //   this.setState({
  //     log: [...this.state.log, data],
  //     value: ''
  //   })
  // }

  // updateTodo = (item) => {
  //   this.setState({
  //     value: item?.text,
  //     btnStatus: item
  //   })
  // }

  // update = () => {
  //   this.props.updateTodo(this.state.btnStatus?.id, this.state.value)
  //   this.setState({
  //     log: [...this.state.log, { id: this.state.btnStatus?.id, status: 'update', text: this.state.value }],
  //     value: '',
  //     btnStatus: {}
  //   })
  // }

  // deleteTodo = () => {
  //   this.props.deleteTodo(this.state.btnStatus?.id)
  //   this.setState({
  //     log: [...this.state.log, { id: this.state.btnStatus?.id, status: 'delete', text: this.state.btnStatus?.text }],
  //     value: '',
  //     btnStatus: {}
  //   })
  // }

  readJsonMessage = (responseBody) => {
    let data;
    try {
        if (typeof responseBody == 'object') {
            data = responseBody;
        } else {
            data = JSON.parse(responseBody);
        }
  
    } catch (e) {
        // Cannot parser message
        return null;
    }
    return data;
  };

  handleSubmit = () => {

    let reqData = {
      path: this.state.path,
      case: this.state.case,
      data: JSON.parse(this.state.dataMock)
    }
    axios.post('http://localhost:9011/api/agent/mockup/create/json', reqData).then(
      response => {
        if (response.status === 200) {
          this.props.apiList()
        }
      })
  }

  handleChange = (e) => {
    this.setState({ method: e.target.value })
  }

  removeApi = (path) => {
    console.log('aaa')
    let reqData = {
      path: path
    }
    axios.post('http://localhost:9011/api/agent/mockup/delete/api',reqData).then(
      response => {
        if (response.status === 200) {
          this.props.apiList()
        }
      })
  }

  render() {
    // const { value, log, btnStatus, typing } = this.state
    const { apiLists } = this.props
    return (
      <div className="main-container">
        <div className='container'>
          <div className='row'>

            <div className="col-12 col-sm-12 col-md-12 col-lg-12">
              <div className="d-flex justify-content-center align-items-center" style={{ marginBottom: 20 }}>
                <img src={logo1} style={{ width: '18vw', maxWidth: 200 }} />
                <img className="logo" src={logo2} style={{ width: '18vw', maxWidth: 165, marginLeft: 14, marginRight: 6 }} />
                <img src={logo3} style={{ width: '18vw', maxWidth: 165 }} />
                <img src={logo4} style={{ width: '18vw', maxWidth: 165 }} />
              </div>
            </div>



            <div className="col-12 col-sm-12 col-md-12 col-lg-6" style={{ marginBottom: 16 }}>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12" style={{ marginBottom: 16 }}>

                <div className="AA">
                  <div style={{ marginBottom: 10, color: 'white', fontSize: 30 }}>
                    <label>
                      Method:
                </label>
                  </div>

                  <div className="BB">
                    {/* <div class="custom-select"> */}
                    {/* onSubmit={this.handleSubmit} */}
                    {/* <form > */}
                    <select style={{ width: 100, height: 35, backgroundColor: '#dc3545', border: 'solid', borderWidth: 1, borderColor: 'white', color: 'white', marginBottom: 10 }}
                      value={this.state.method} onChange={(e) => this.handleChange(e)}>
                      <option value="POST">POST</option>
                      <option value="GET">GET</option>
                      <option value="PUT">PUT</option>
                      <option value="DELETE">DELETE</option>
                    </select>

                    <div className="row">
                      <div className="col-12">
                        <div style={{ marginBottom: 5, color: 'white' }}>
                          <label style={{ color: 'white' }}>PATH</label>
                        </div>
                        <input type="text" id="subdomaintwo" value="http://localhost:3000/" disabled />
                        <input type="text" placeholder="ExampleDomain" id="subdomain" onChange={(event) => {
                          this.setState({ path: event.target.value })
                        }} />
                      </div>
                      <div className="col-3" style={{ marginBottom: 20 }}>
                        <div style={{ marginBottom: 5, color: 'white' }}>
                          <label style={{ color: 'white' }}>CASE</label>
                        </div>
                        <input style={{ fontSize: 18, width: '50%' }} type="text" value={this.state.case} onChange={(event) => {
                          this.setState({ case: event.target.value })
                        }}
                        />
                      </div>
                      <div className="col-12">
                        <input style={{ backgroundColor: '#28a745', color: '#fff' }} type="submit" value="Submit" onClick={() => this.handleSubmit()} />
                      </div>
                    </div>
                    {/* </form> */}
                    {/* </div> */}
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12">

                <div className="AA">
                  <textarea style={{ width: '100%', background: '#282b2f', color: 'white', resize: 'none' }}
                    id="textarea"
                    name="textarea"
                    rows="40" cols="40"
                    value={this.state.dataMock}
                    onChange={(event) => {
                      this.setState({ dataMock: event.target.value })
                    }}
                  >
                  </textarea>
                </div>
              </div>

            </div>
            <div className="col-12 col-sm-12 col-md-12 col-lg-6" style={{ marginBottom: 16 }}>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                <div className="AA scoll">
                  <div className="title" style={{ marginBottom: 8, borderBottonStyle: 'soild' }}><label style={{ fontSize: 20 }}>API LIST</label> </div>
                  {
                    apiLists.map((path, index) => (
                      <div className={`api-list-container user-wrapper py-1  ${index === this.state.api ? 'active' : ''}`}>
                      <div className={`api-box`} onClick={() => this.handleselect(path, index)}>
                        <img src={circle} style={{ width: '18vw', maxWidth: 20, marginRight: 10 }} />
                        <label style={{ fontSize: 15 }}>{path.api}</label>
                      </div>
                      <div onClick={()=>this.removeApi(path)}>
                           <img src={remove} style={{ width: '18vw', maxWidth: 20, marginRight: 10 }} />
                      </div>
                      </div>
                    ))
                  }
                </div>
              </div>
            </div>

            {/* <div className='left'>
            <span>Todo List</span>
            <div className='blog'>
              <ul>
                {todo && todo.map((item, i) => <li onClick={() => this.updateTodo(item)} key={i}>{item.text}</li>)}
              </ul>
            </div>
          </div>

          <div className='mid'>
            <span>Task Name</span>
            <input type='text' value={value} onChange={(e) => this.handleChange(e)}
              onFocus={() => this.setState({ typing: true })}
              onBlur={() => this.setState({ typing: false })}
            />
            <div className='typingDiv'>
              {typing && <p className='typing'>typing ...</p>}
            </div>

            <button className='buttonCreate' onClick={() => !btnStatus.id ? this.create() : this.update()}>{!btnStatus.id ? 'CREATE' : 'UPDATE'}</button>
            <button className='button' onClick={() => this.deleteTodo()}>DELETE</button>
          </div>

          <div className='right'>
            <span>Activity List</span>
            <div className='blog'>
              <ul>
                {log?.map((item, i) => <li key={i}>{item?.status}-{item?.text}</li>)}
              </ul>
            </div>
          </div> */}
          </div>
        </div>
      </div >
    )
  }
}

const mapStateToProps = (state) => {
  return {
    apiLists: state.apiList.apiList
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    apiList: () => {
      dispatch(getApiListAction())
    },
    createTodo: (data) => {
      dispatch(createTodoAction(data))
    },
    updateTodo: (id, value) => {
      dispatch(updateTodoAction(id, value))
    },
    deleteTodo: (id) => {
      dispatch(deleteTodoAction(id))
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(App)
